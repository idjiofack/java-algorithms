
public class Node implements Comparable<Node> {
	int number;
	int cost;
	public Node(int number, int cost){
		this.number=number;
		this.cost=cost;
	}
	@Override
	public int compareTo(Node o) {
		// TODO Auto-generated method stub
		 return this.cost-o.cost;
	}
}
