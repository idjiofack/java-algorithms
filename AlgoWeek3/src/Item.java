


public class Item implements Comparable<Item> {
 private long value;
 private int weight;
 public static int capacity=0;
 public static int numItems=0;
 
 public Item(long value, int weight){
	 this.value=value;
	 this.weight= weight;
	 
 }
public long getValue() {
	return value;
}
public void setValue(long value) {
	this.value = value;
}
public int getWeight() {
	return weight;
}
public void setWeight(int weight) {
	this.weight = weight;
}
@Override
public int compareTo(Item o) {
	// TODO Auto-generated method stub
	int result=1;
	if(this.weight>o.weight) result=-1;
	return result;
	
}

@Override
public String toString(){
	String ch="value " + value + "weigt"+ weight;
	return ch;
}
	
}
