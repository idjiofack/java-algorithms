import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class GraphUtils {
	
	public static int edges=0;
	public static int  vertices=0;
	
	private static LinkedList<Edge> getDestinationV(int ed){
		LinkedList<Edge> list= new LinkedList<>();
		for (Iterator<Edge> iterator = Main.listEdges.iterator(); iterator.hasNext();) {
			Edge edge = (Edge) iterator.next();
			if(edge.getDestination()==ed)
				list.add(edge);			
		}
		return list;
	}
	
	public static Edge belong(int node1, int node2){
		for (Iterator<Edge> iterator = Main.listEdges.iterator(); iterator.hasNext();) {
			Edge edge = (Edge) iterator.next();
			if((edge.getSource()==node1)&&(edge.getDestination()==node2))
				return edge;			
			
		}
		return null;
	}
	public static long getPairs(){
		//Initialize the array
		long[][] original = new long[vertices][vertices];
		original = Main.pairGraph;	
		
		// main algorithm
		int k=0;
		for ( k = 0; k < vertices; k++) {
			for (int i = 0; i < vertices; i++) {
				for (int j = 0; j < vertices; j++) {			
					
					
					Main.pairGraph[i][j]=Math.min(Main.pairGraph[i][j],Main.pairGraph[i][k]+Main.pairGraph[k][j] );
				}
				
			}
			
			System.out.println("This is the value of k= "+ k);
			
		}
		long minValue= 1000000;
		if(k==vertices){
			for (int i = 0; i < vertices; i++) {
				for (int j = 0; j < vertices; j++) {
					System.out.println("min values "+ minValue);
					minValue= Math.min(minValue, Main.pairGraph[i][j]);
				}
				
			}
		}
		
		return minValue;
		
	}
	public static long getCyle(){
		//initialise the base case with source 0
		//int source=0;
		//add the last extra node
		
		long min= 1000000;
		/*for (int i = 0; i < vertices; i++) {
			Main.listEdges.add(new Edge(vertices, i, 0));
		}*/
		
		for(int source=0; source<vertices; source++){
		for (int i = 0; i < vertices; i++) {
			if(i==source)Main.graph[0][i]=0;
			else Main.graph[0][i]=1000000;
		}
		
		
		for (int i = 1; i < vertices-1; i++) {
			for(int j = 0; j<vertices; j++){
				LinkedList<Edge> list = getDestinationV(j);
				long minDist=1000000;
				for (Edge edge : list) {
					minDist= Math.min(minDist,Main.graph[i-1][edge.getSource()]+edge.getCost());
				}
				Main.graph[i][j]=Math.min(minDist,Main.graph[i-1][j] );
				System.out.println("The value of the stuff is k=" +j);
				
			}
			System.out.println("The value of the stuff is k=" +i);
			if(i==vertices-2){
				// if we have just handled the last vertex, I'll store the diagonale.
				for (int j = 0; j < vertices; j++) {
					if(source!=j)min=Math.min(min,Main.graph[i][j] );
				}
			}
			/*if(i==vertices-1){
				for (int j = 0; j < vertices; j++) {
					if(diagonal[j]!=Main.graph[i][j]) return true;
				}
			}*/
			
			}
		}
		return min;
		
	}
	
	public static LinkedList<Edge> loadData(String filename){
		LinkedList<Edge> list= new LinkedList<>();
		Path source = Paths.get(filename);
		int i=0;
		Charset charset= Charset.forName("US-ASCII");
		try(
				BufferedReader reader = Files.newBufferedReader(source, charset)
				
				)
		
		{
			String line= null;
			
			while ((line= reader.readLine()) != null) {
				//handle the first line
				if(i==0){
					//get the capacity and the number of items
					String[] values=line.split(" ");
					vertices=Integer.parseInt(values[0]);
					edges= Integer.parseInt(values[1]);
					Main.graph = new long[vertices][vertices+1];
					Main.pairGraph = new long[vertices][vertices];
					i++;
					for (int j = 0; j < vertices; j++) {
						for (int h = 0; h < vertices; h++) {
							if(j==h)Main.pairGraph[j][h]=0;
							Main.pairGraph[j][h]=1000000;
						}
					}
					
					
				}
				else{
					//gets the value and the weight of an array and stores it in an array
					
					String[] values=line.split(" ");
					 int src = Integer.parseInt(values[0]);
					int dest= Integer.parseInt(values[1]);
					long value= Long.parseLong(values[2]);
					Main.pairGraph[src-1][dest-1]=value;
					//list.add(new Edge(src-1, dest-1, value));
									
				}
					
					
				}
				
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return list;
		
	}

}
