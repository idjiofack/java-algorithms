package datastructure.main;

import java.util.LinkedList;
import java.util.PriorityQueue;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import datastructure.unionfind.Edge;
import datastructure.unionfind.Node;
import datastructure.unionfind.UnionFind;
import datastructure.util.Utils;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Program ended");
		UnionFind uf = new UnionFind();
		PriorityQueue<Edge> sortedNodes;
		
		
		
		sortedNodes= Utils.loadGraph("clustering1.txt", uf);

		System.out.println("Program ended tell me the size of that guy "+ sortedNodes.size());
		while(uf.getNumClusters()>4){
			
			Edge a= sortedNodes.poll();			
			uf.Union(a.getDep(), a.getArr());
		}
		
		System.out.println("Program ended tell me the size of that guy "+ uf.getClusters().size());
		
	LinkedList<Edge> clustersDistance = new LinkedList<>();
	long maxCost=0;
	while((clustersDistance.size()<6)&&(sortedNodes.size()>0)){
		Edge a= sortedNodes.poll();
		Node depart= uf.find(a.getDep());
		Node arrive= uf.find(a.getArr());
		
		if(depart!=arrive){
			// the two nodes belong to different clusters, set their distances if necessary
			if(!Utils.exists(depart, arrive, clustersDistance)) clustersDistance.add(new Edge(depart,arrive,a.getCost()));
			maxCost= a.getCost();
		}
		
		
	}
		// take the maximum of the cluster array!
	
	for (Edge edge : clustersDistance) {
		if(edge.getCost()<maxCost)maxCost=edge.getCost();
	}
	
	System.out.println("Program is on an end the maximum distance is  "+ maxCost);
		

	}

}
