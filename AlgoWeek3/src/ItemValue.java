
public class ItemValue {
	
	private long value;
	private long capacity;
	
	public ItemValue(long value, long capacity){
		this.value=value;
		this.capacity=capacity;
	}
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	public long getCapacity() {
		return capacity;
	}
	public void setCapacity(long capacity) {
		this.capacity = capacity;
	}
	

}
