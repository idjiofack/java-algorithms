package datastructure.util;


import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import datastructure.unionfind.*;

public class Utils {
	
	public static PriorityQueue<Edge> loadGraph( String filename, UnionFind uf){
		PriorityQueue<Edge>  orderedArray = new PriorityQueue<>();
		LinkedList<Node> universe= new LinkedList<>();
		Path source = Paths.get(filename);
		int i=0;
		Charset charset= Charset.forName("US-ASCII");
		try(
				BufferedReader reader = Files.newBufferedReader(source, charset)
				
				)
		
		{
			String line= null;
			
			while ((line= reader.readLine()) != null) {
				//handle the first line
				if(i==0){
					//get the number of nodes and create the universe for the union find data structure
					int nodes= Integer.parseInt(line);
					for(int j=0;j< nodes; j++){
						universe.add(new Node(j+1));
					}
					uf.setUniverse(universe);
					i++;
				}
				else{
					//Edges processing, get the nodes from the universe, use them to create Edges and store them in the priority queue
					
					String[] values=line.split(" ");
					int depNode=Integer.parseInt(values[0]);
					int arrNode= Integer.parseInt(values[1]);
					int cost= Integer.parseInt(values[2]);
					
					Node dep= uf.getNodeByLabel(depNode);
					Node arr= uf.getNodeByLabel(arrNode);
					
					orderedArray.add(new Edge(dep,arr,cost));
					
				}
					
					
				}
				
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return orderedArray;
		
	}
	
	public static boolean exists(Node one, Node two, LinkedList<Edge> list){
		
		Iterator<Edge> iterator= list.iterator();
		
		while(iterator.hasNext()){
			Edge e= iterator.next();
			if( (one==e.getArr()||(one==e.getDep()))&&(two==e.getArr()||(two==e.getDep())))
				return true;
		}
		return false;
	}
}
