import java.util.Iterator;
import java.util.PriorityQueue;


public class Edge implements Comparable<Edge> {
	int nodeLabel;
	int parent;
	 long cost;
	 public Edge(int nodeLabel, int parent, int cost){
		 this.nodeLabel= nodeLabel;
		 this.parent=parent;
		 this.cost= cost;
	 }
	@Override
	public int compareTo(Edge o) {
		// TODO Auto-generated method stub
		int result=-1;
		if(this.cost>o.cost) result=1;
		return result;
		
	}
	public long getCost() {
		return cost;
	}
	
	public void setCost(long cost) {
		this.cost = cost;
	}
	public int getNodeLabel() {
		return nodeLabel;
	}
	
	public int getParent() {
		return parent;
	}
	
	public void setNodeLabel(int nodeLabel) {
		this.nodeLabel = nodeLabel;
	}
	
	public void setParent(int parent) {
		this.parent = parent;
	}
	
	@Override
	public String toString(){
		String sh= "parent: "+parent+ "nom: "+nodeLabel+ " distance "+cost;
		return sh;
	}
	
}


