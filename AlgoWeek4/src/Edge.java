
public class Edge {

private int source;
private int destination;
private long cost;

public Edge(int source, int destination, long cost){
	this.source= source;
	this.destination=destination;
	this.cost= cost;
}

public int getSource() {
	return source;
}
public void setSource(int source) {
	this.source = source;
}
public int getDestination() {
	return destination;
}
public void setDestination(int destination) {
	this.destination = destination;
}
public long getCost() {
	return cost;
}
public void setCost(long cost) {
	this.cost = cost;
}




}
