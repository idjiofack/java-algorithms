


public class Task implements Comparable<Task> {

		int length;
		int weight;
		double priority;
		double completionTime;
		public Task(int length, int weigth){
			this.length=length;
			this.weight=weigth;
			this.completionTime=0;
			priority= computePriority();
			
		}
		public double computePriority(){
			return weight- length;
		}
		
		@Override
		public String toString(){
			
			String result="";
			result= "length :"+ this.length+"; weight:"+ this.weight+ "; completion time:"+this.completionTime+"; priority:"+ this.priority; 
			
			return result;
		}
		@Override
		public int compareTo(Task o) {
			// TODO Auto-generated method stub
			// result will be negative if object is less than the argument
			int result=-1;
			if(this.priority<o.priority) result=1;
			
			//breaking ties
			if(this.priority == o.priority){
				if(this.weight>o.weight) result=-1;
				else result = 1;
			}
			return result;
		}
}
