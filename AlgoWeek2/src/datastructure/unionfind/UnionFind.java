package datastructure.unionfind;

import java.util.Iterator;
import java.util.LinkedList;

public class UnionFind {
	private LinkedList<Node> universe;
	//number of distinct sets.
	private int numClusters;
	
	public UnionFind(){
		setUniverse(new LinkedList<Node>());
		setNumClusters(0);
		
	}

	public LinkedList<Node> getUniverse() {
		return universe;
	}

	public void setUniverse(LinkedList<Node> linkedList) {
		this.universe = linkedList;
		setNumClusters(universe.size());
		
	}
	
	// Operations
	
	public void Union(Node a, Node b){
		
		Node s1 = find(a);		
		Node s2 = find(b);
		if(s1!=s2){
			if(s1.getRank()<s2.getRank())
					s1.setParent(s2);
			if(s1.getRank()>s2.getRank())
				s2.setParent(s1);
			if(s1.getRank()==s2.getRank()){
				s2.setParent(s1);
				s1.setRank(s1.getRank()+1);
			}
			
			this.setNumClusters(numClusters-1);
		}
		
		}
	
	public Node find(Node a){
		Node b=a;
		while(b.getParent()!=b){
			
			b=b.getParent();
		}
		return b;
	}
	
	public Node getNodeByLabel(int label){
		
		Iterator<Node> iterator= universe.iterator();
		
		while(iterator.hasNext()){
			Node a = iterator.next();
			if(a.getLabel()==label) return a;
		}
		return null;
	}

	public int getNumClusters() {
		return numClusters;
	}

	public void setNumClusters(int numClusters) {
		this.numClusters = numClusters;
	}
	
	public LinkedList<Node> getClusters(){
		LinkedList<Node> ans= new LinkedList<Node>();
		for (Node node : universe) {
			Node ancestor= find(node);
			if(!ans.contains(ancestor)) ans.add(ancestor);
		}
		return ans;
	}
			
	}


