package datastructure.unionfind;

public class Node {
		private int rank;
		private Node parent;
		private int label;
		
		public Node(int label){
			setRank(0);
			setParent(this);
			this.setLabel(label);	

			
		}

		public int getRank() {
			return rank;
		}

		public void setRank(int rank) {
			this.rank = rank;
		}

		public Node getParent() {
			return parent;
		}

		public void setParent(Node parent) {
			this.parent = parent;
		}

		public int getLabel() {
			return label;
		}

		public void setLabel(int label) {
			this.label = label;
		}

}
