
public class City {
	
	private long xCoord;
	private long yCoord;
	public City(long xCoord, long yCoord){
		this.setxCoord(xCoord);
		this.setyCoord(yCoord);
	}
	public long getxCoord() {
		return xCoord;
	}
	public void setxCoord(long xCoord) {
		this.xCoord = xCoord;
	}
	public long getyCoord() {
		return yCoord;
	}
	public void setyCoord(long yCoord) {
		this.yCoord = yCoord;
	}

}
