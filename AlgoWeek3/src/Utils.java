import java.awt.ItemSelectable;
import java.io.BufferedReader;
import java.io.ObjectInputStream.GetField;
import java.net.NetworkInterface;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;


public class Utils {
	
	private static  Long getSolution(int i, int x ){
		int key= i*Item.numItems + x;
		return Utils.solutionSet.get(key);
	}
	
	private static void setSolution(int i, int x, Long value){
		int key= i*Item.numItems + x;
		Utils.solutionSet.put(key, value);
	}
	
	private static void setSolution1(int i, long x, long value){
		long key= i*Item.numItems + x;
		Utils.solutionSet1.put(key, value);
	}
	
	
	private static  long getSolution1(int k, long x ){
	 LinkedList<ItemValue> capacityList=	keepingTrackTable.get(k);
	 
	 
	 for (int i=capacityList.size()-1;i>=0; i--) {
		 ItemValue itemValue= capacityList.get(i);
		if(itemValue.getCapacity()==x) return itemValue.getValue();
		if(itemValue.getCapacity()<x) return 0;
	}
	
	 return 0;
	}
	
	
	
	public static Hashtable<Integer, Long> solutionSet = new Hashtable<>();
	public static Hashtable<Long, Long> solutionSet1 = new Hashtable<>();
	
	// this table keeps track of (item,capacity) couple for each item it maintains the list of capacities for wich values have been computed
	public static Hashtable<Integer, LinkedList<ItemValue>> keepingTrackTable = new Hashtable<>();
	
	public static void updateTrackPrevious(int item, ItemValue value){
		LinkedList<ItemValue> cap=	keepingTrackTable.get(item);
		
		
	}
	
	public static void updateTrackTable1(int item, long capacity, long value){
		 LinkedList<ItemValue> cap=	keepingTrackTable.get(item);	
			
		 if(cap==null){
			 cap = new LinkedList<>();
			 ItemValue itemValue= new ItemValue(value, capacity);			
			 cap.add(itemValue);
			 keepingTrackTable.put(item, cap);
		 }
		 else{
			 //find the position where the new item will be added
			 int addingIndex=0;
			 int indexLastvalue= cap.size();			 
			 for (int i = indexLastvalue; i >0 ; i--){
				 if(cap.get(i-1).getCapacity()<=capacity){
					 addingIndex=i;
					 break;
				 }
			 }
			 
			 if(cap.get(addingIndex-1).getCapacity()==capacity){
				 // on est sur la meme capacite mettre a jour
				 ItemValue itemEqual= cap.get(addingIndex-1);
				 itemEqual.setValue(Math.max(itemEqual.getValue(), value));
			 }
			 else{
				 if(cap.get(addingIndex-1).getValue()< value){
					 ItemValue toAdd= new ItemValue(value, capacity);
					 cap.add(addingIndex,toAdd);
					 //remove any item that violates the condition
					
					 for(int j=addingIndex+1; j< cap.size(); j++){							 
						 if(cap.get(j).getValue()<= value){
							 cap.remove(j);
						 }
						 
						 else{
							 break;
						 }
					 }
				 }
			 }
			 
		 }
		
	}
	
	private static void updateTrackTable12(int item, long capacity, long value){
		 LinkedList<ItemValue> cap=	keepingTrackTable.get(item);	
		
		 //if it is the first item to be added add it!
		 if(cap==null){
			 cap = new LinkedList<>();
			 ItemValue itemValue= new ItemValue(value, capacity);			
			 cap.add(itemValue);
			 keepingTrackTable.put(item, cap);
		 }
		 else{
			 int indexLastvalue= cap.size()-1;
			 ItemValue lastItem= cap.get(indexLastvalue);			 
			 if((value<lastItem.getValue())&&(capacity<lastItem.getCapacity())){
				 
				 for (int i = indexLastvalue; i >0 ; i--) {
					if((cap.get(i-1).getCapacity()<=capacity)){//||(cap.get(i-1).getValue()==value)
						//if(cap.get(i-1).getValue()!=value){
							if(cap.get(i-1).getCapacity()<capacity){
							ItemValue itemValue = new ItemValue(value,capacity);
							cap.add(i, itemValue);
							break;
							}
							else{
								ItemValue itemEqual= cap.get(i-1);
								itemEqual.setValue(Math.max(itemEqual.getValue(), value));
								break;
							}
						//}
						/*else{
							//remove the value make sure you keep the smallest!
							//the currenvaly has the same value as the predecessor, but a
							if(cap.get(i-1).getCapacity()<capacity){
								cap.remove(i-1);
								}
							i--;
						}*/
					}
				}	
			 }
			 if((value==lastItem.getValue())&&(capacity<lastItem.getCapacity())){
				 ItemValue itemValue = new ItemValue(value,capacity);
				 cap.remove(lastItem);
				 cap.add(indexLastvalue, itemValue);
			 }
			 
			 if( (value>lastItem.getValue())&&(capacity>=lastItem.getCapacity())){
				 if(capacity==lastItem.getCapacity()){
					 lastItem.setValue(Math.max(lastItem.getValue(), value));
				 }
				 else{
					 ItemValue itemValue = new ItemValue(value,capacity);				 
					 cap.add(indexLastvalue+1, itemValue);
				 }
			 }
				 
			 
					
				}
				 // we are about to add the highest capacity so far!
				
				 
			}
			 
		 
			 	
		
		
		
	
	private static void updateTrackTable(int item, long capacity, long value){
	 LinkedList<ItemValue> cap=	keepingTrackTable.get(item);	
	 
	 //if it is the first item to be added add it!
	 if(cap==null){
		 cap = new LinkedList<>();
		 ItemValue itemValue= new ItemValue(value, capacity);
		 cap.add(itemValue);
		 keepingTrackTable.put(item, cap);
	 }
	 else{
		 //not the first capacity to added likely to be case after initialization
		 boolean found = false;
		 // has a value of this capacity has been added previously?
		 for ( int j = 0; j < cap.size(); j++) {
			 // has a value of this capacity has been added previously?
			 ItemValue itemValue= cap.get(j);
			 if(itemValue.getCapacity()==capacity){
				// the answer is yes!
				 found = true;				 
			 }
			 if(found==true){
				// if the answer is yes update only if it is a better opportunity
				if(itemValue.getValue()<value) itemValue.setValue(value);
				//if it is not the last element update the following capacities.
			/*	if(j!= cap.size()-1){
				ItemValue nextValue= cap.get(j+1);
				if(nextValue.getValue()<= itemValue.getValue())cap.remove(nextValue);
				}*/
				
				while(j< cap.size()-1){
					ItemValue nextValue= cap.get(j+1);
					long max= itemValue.getValue()- nextValue.getValue();
					if(max <0) j=cap.size()-1;
					else{
						cap.remove(nextValue);
						j++;
					}
					
				}
			/*	while(j!= cap.size()-1){
					ItemValue nextValue= cap.get(j+1);
					long max= Math.max(itemValue.getValue(), nextValue.getValue());
					if(nextValue.getValue()==max) j=cap.size()-1;
					else{
						nextValue.setValue(max);
						j++;
					}
					
					
				}*/
				break;
			 }
			 }
		 		 
			 
		 
		 // no this capacity for this item has not been added yet.
		 if(found==false){
			 ItemValue itemValue= new ItemValue(value, capacity);
			 int n=cap.size();
			 int i=0;
			 //let's find the right position where to add it!
			 for (  i = 0; i < cap.size(); i++) {
				ItemValue currentItem= cap.get(i);
				if(currentItem.getCapacity()>capacity){					
					//si la valeur pour la capacite superieur est inferieure a la valeur de la capacite inferieure ne pas ajouter!
					if(cap.get(i-1).getValue()<value){
						// let's add the item and check his successor to see if we can update it!
						cap.add(i, itemValue);
						//ItemValue nextValue= cap.get(i+1);
						int j=i;
						while(j< cap.size()-1){
							ItemValue nextValue= cap.get(j+1);
							long max= itemValue.getValue()- nextValue.getValue();
							if(max<0) j=cap.size()-1;
							else{
								cap.remove(nextValue);
								j++;
							}
						}
						
						/*while(j!= cap.size()-1){
							ItemValue nextValue= cap.get(j+1);
							long max= Math.max(itemValue.getValue(), nextValue.getValue());
							if(nextValue.getValue()==max) j=cap.size()-1;
							else{
								nextValue.setValue(max);
								j++;
							}
							
							//if(nextValue.getValue()<= itemValue.getValue())cap.remove(nextValue);
						}*/
						//nextValue.setValue(Math.max(itemValue.getValue(), nextValue.getValue()));
						//if(nextValue.getValue()<= itemValue.getValue())cap.remove(nextValue);
					}
					break;				
				}
				
			}
			 // we are about to add the highest capacity so far!
			 if(i==n){
				 if(cap.get(n-1).getValue()<value)
						cap.add(n, itemValue);
				 else {
					 value= cap.get(n-1).getValue();
					 itemValue.setValue(value);
					 cap.add(n,itemValue);
				 }
			 }
			 
		}
		 
	 }
		 	
	}
	
	
	
	public static LinkedList<Item> loadData(String filename){
		LinkedList<Item> items = new LinkedList<>();
		Path source = Paths.get(filename);
		int i=0;
		Charset charset= Charset.forName("US-ASCII");
		try(
				BufferedReader reader = Files.newBufferedReader(source, charset)
				
				)
		
		{
			String line= null;
			
			while ((line= reader.readLine()) != null) {
				//handle the first line
				if(i==0){
					//get the capacity and the number of items
					String[] values=line.split(" ");
					int capacity=Integer.parseInt(values[0]);
					int numItems= Integer.parseInt(values[1]);
					Item.capacity=capacity;
					Item.numItems=numItems;
					i++;
					
				}
				else{
					//gets the value and the weight of an array and stores it in an array
					
					String[] values=line.split(" ");
					long value= Long.parseLong(values[0]);
					int weight= Integer.parseInt(values[1]);
					Item item= new Item(value, weight);
					
					items.add(item);
									
				}
					
					
				}
				
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return items;
		
	}
	
	
	
	public static LinkedList<Item> loadDataTwo(String filename){
		LinkedList<Item> items = new LinkedList<>();
		PriorityQueue<Item> sortedItems=  new PriorityQueue<>();
		Path source = Paths.get(filename);
		int i=0;
		Charset charset= Charset.forName("US-ASCII");
		try(
				BufferedReader reader = Files.newBufferedReader(source, charset)
				
				)
		
		{
			String line= null;
			
			while ((line= reader.readLine()) != null) {
				//handle the first line
				if(i==0){
					//get the capacity and the number of items
					String[] values=line.split(" ");
					int capacity=Integer.parseInt(values[0]);
					int numItems= Integer.parseInt(values[1]);
					Item.capacity=capacity;
					Item.numItems=numItems;
					i++;
					
				}
				else{
					//gets the value and the weight of an array and stores it in an array
					
					String[] values=line.split(" ");
					long value= Long.parseLong(values[0]);
					int weight= Integer.parseInt(values[1]);
					Item item= new Item(value, weight);
					
					sortedItems.add(item);
									
				}
					
					
				}
			
			while(sortedItems.size()>0){
				items.add(sortedItems.poll());
			}
				
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return items;
		
	}
	
	
	public static void partOneBigf(){
	LinkedList<Item> items= Utils.loadDataTwo("testcase");
		
		
		//Initialization!
	    display(items);
		
		for (int k = 0; k <Item.numItems; k++) {
			Item item= items.get(k);
			long capacity=Item.capacity;
			int itemIndex=Item.numItems-1;
			while(capacity >=item.getWeight() ){
								
				long capacity1 = capacity-item.getWeight();	
				Utils.setSolution1(k, capacity,Math.max(Utils.getSolution1(k-1, capacity),Utils.getSolution1(k-1, capacity1)+item.getValue()));
				//System.out.println("item "+ k+ "capacity=" + capacity +"value= "+Utils.getSolution1(k, capacity));
				capacity= capacity - items.get(itemIndex).getWeight();
				itemIndex--;
				
				
			}				
			
		}
		
		System.out.println("this is the capacity: "+ getSolution1(Item.numItems-1,Item.capacity));
	}
	
	public static void display(LinkedList<Item> items){
		int i=0;
		while(i<items.size()){
			System.out.println("Item #"+i+ " "+items.get(i));
			i++;
		}
	}
	
	
	
	
	public static void partOneBigNo(){
		LinkedList<Item> items= Utils.loadData("testcase");
			
			System.out.println("this is the capacity: "+ Item.capacity);
			
			
			for (int k = 0; k <Item.numItems; k++) {
					for (int j = 0; j <Item.capacity+1; j++) {
						Item item= items.get(k);
						int xIndex= j-item.getWeight();
						if(xIndex<0){
							xIndex=j; //A[k][j]=A[k-1][j];
							Utils.setSolution1(k, j, Utils.getSolution1(k-1, j));
						}
						else{
							//if(A[k-1][j]>(A[k-1][xIndex]+item.getValue())) A[k][j]=A[k-1][j];
							//else A[k][j]=A[k-1][xIndex]+item.getValue();
							Utils.setSolution1(k, j,Math.max(Utils.getSolution1(k-1, j),Utils.getSolution1(k-1, xIndex)+item.getValue()));

						}
					}
				
			}
			
			System.out.println("this is the capacity: "+ getSolution1(Item.numItems-1,Item.capacity));
		}


	public static void partOneBig(){
		LinkedList<Item> items= Utils.loadDataTwo("testcase");
			
			System.out.println("this is the capacity: "+ Item.capacity);
			// initialize capacity=0 for all the i first
			for (int i = 0; i < Item.numItems+1; i++) {
					
				updateTrackTable1(i,0,0);
			}
			
			
			for (int k = 1; k <Item.numItems+1; k++) {
				Item current= items.get(k-1);
				// get all the capacities you can get from k-1
				// for each of them,compute values of k column based on previous capacity(k-1)+current item weight 
				
				LinkedList<ItemValue> e=keepingTrackTable.get(k-1);
				for(ItemValue previous : e){
					if(previous.getCapacity()!=0)updateTrackTable1(k, previous.getCapacity(), previous.getValue());
				}
				//System.out.println("k"+ e.size());
				for (ItemValue cap1 : e) {
					//check a condtion before doing this
					
					long capacity= cap1.getCapacity();					
					long newCap=capacity+ current.getWeight();
					long newVal= getSolution1(k-1, capacity)+current.getValue();					
					if(newCap> Item.capacity) break;	
					updateTrackTable1(k, newCap, newVal);	
					//int j=k+1;
					
					
				}
				
				System.out.println("traking table at layer  "+ k+"--------------------------------");
				LinkedList<ItemValue> results= keepingTrackTable.get(k);
				
				for (Iterator<ItemValue> iterator = results.iterator(); iterator.hasNext();) {
					ItemValue itemValue = (ItemValue) iterator.next();
					
					System.out.println("capacity "+ itemValue.getCapacity()+" value "+ itemValue.getValue());
				}
				
				System.out.println("traking table at layer  "+ k+" Fin--------------------------------");
				
				
			}
			
			LinkedList<ItemValue> v= keepingTrackTable.get(Item.numItems);
			
			System.out.println("this is the capacity: "+ v.get(v.size()-1).getValue());
		}


	
	
	public static void partOne(){
		LinkedList<Item> items= Utils.loadData("testcase");
			
			System.out.println("this is the capacity: "+ Item.capacity);
			
			long[][] A= new long[Item.numItems+1][Item.capacity+1];
			
			
			//Initialization!
			for (int i = 0; i < Item.capacity+1; i++) {
				A[0][i]=0;
				
			}
			
			for (int k = 1; k <Item.numItems+1; k++) {
					for (int j = 0; j <Item.capacity+1; j++) {
						Item item= items.get(k-1);
						int xIndex= j-item.getWeight();
						if(xIndex<0){
							xIndex=j; A[k][j]=A[k-1][j];
						}
						else{
							if(A[k-1][j]>(A[k-1][xIndex]+item.getValue())) A[k][j]=A[k-1][j];
							else A[k][j]=A[k-1][xIndex]+item.getValue();
						}
					}
				//	System.out.println("traking table at layer  "+ k+"--------------------------------");
					//LinkedList<ItemValue> results= keepingTrackTable.get(k);
					
					//for (int z=0; z<Item.capacity+1 ; z++) {
						
					//	System.out.println("capacity "+ z +" value "+ A[k][z] );
					//}
					
					//System.out.println("traking table at layer  "+ k+" Fin--------------------------------");
			}
			
			System.out.println("this is the capacity: "+ A[Item.numItems][Item.capacity]);
		}

		
	
	
	public static long knapsack(int i, int x){
		if(i==0|| x==0){
			
			return 0;
		}
		else{
			if(Utils.getSolution(i, x)!=null){
				return Utils.getSolution(i, x);
			}
			else{
				Item item=Main.items.get(i-1);
				int xIndex= x-item.getWeight();
				long k=knapsack(i-1,x);
				if(xIndex<0) {
					//do I need to update the table?
					Utils.setSolution(i, x,k );
					
				}				
				
				else{
					
					Utils.setSolution(i, x,Math.max(k, knapsack(i-1,xIndex)+item.getValue()));
				}
				System.out.println("\t just to see how it works1 "+ Utils.getSolution(i, x));
				return Utils.getSolution(i, x);	
			}
			
			
		}
	}
}
