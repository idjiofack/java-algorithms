package datastructure.unionfind;



public class Edge implements Comparable<Edge> {
	private Node dep;
	private Node arr;
	private long cost;
	public Edge(Node a, Node b, long cost){
		setDep(a);
		setArr(b);
		this.setCost(cost);
	}
	
	@Override
	public int compareTo(Edge o) {
		// TODO Auto-generated method stub
		int result=-1;
		if(this.cost>o.cost) result=1;
		return result;
		
	}
	public Node getDep() {
		return dep;
	}
	public void setDep(Node dep) {
		this.dep = dep;
	}
	public Node getArr() {
		return arr;
	}
	public void setArr(Node arr) {
		this.arr = arr;
	}
	public long getCost() {
		return cost;
	}
	public void setCost(long cost) {
		this.cost = cost;
	}

	@Override
	public String toString(){
		String ch= "dep "+ dep +" arr "+ arr+ " cost "+ cost;
		return ch;
	}
}
