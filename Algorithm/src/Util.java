import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.PriorityQueue;


public class Util {


	/**
	 * @param args
	 */
	
		public static PriorityQueue<Task> loadTask(String filemane){
			
			PriorityQueue<Task> q = null;
			int i=0;
			Path source = Paths.get(filemane);
			Charset charset= Charset.forName("US-ASCII");
			try(
					BufferedReader reader = Files.newBufferedReader(source, charset)
					
					)
			
			{
				String line= null;
				while ((line= reader.readLine()) != null) {
					//System.out.println(line);
					
					//lines.add(line);
					// create and add tasks to the priority queue
						if(i==0){
							//read the first line of the file and initialize the queue							
							q= new PriorityQueue<>(11);
							i++;
						}
						else{
							String[] values=line.split(" ");
							Task t= new Task(Integer.parseInt(values[1]),Integer.parseInt(values[0]));
							q.add(t);
						}
						
					}
					
				
				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
			}
			return q;
	}
		
	public static long computeCompletionTime(PriorityQueue<Task> q){
		long completionTime=0;
		long averageCompletionTime=0;
		Path target = Paths.get("result.txt");
		Charset charset= Charset.forName("US-ASCII");
		
try ( BufferedWriter writer= Files.newBufferedWriter(target, charset))
		
		
		{
	String s = String.valueOf(q.size());
	writer.append(s, 0, s.length());
	writer.newLine();
			while(!q.isEmpty()){
				
				Task t= q.poll();
				completionTime += t.length;
				t.completionTime = completionTime;
				averageCompletionTime+= t.weight*t.completionTime;
				System.out.println(t);
				writeOutput(writer,t);
				
			}
			
		    s = " weighted Completion time ="+String.valueOf(averageCompletionTime);
			writer.append(s, 0, s.length());
			writer.newLine();
		}
	catch (Exception e) {
		// TODO: handle exception
	}
		return averageCompletionTime;
	}
	
	private static void writeOutput(final BufferedWriter writer,  Task t) throws IOException{

			
				String s= t.weight+" "+t.length +" Priority: "+ t.priority+ "  completion Time: "+ t.completionTime;
				writer.append(s, 0, s.length());
				writer.newLine();
			
		

	}
	
	public static int averageCompletionTime(PriorityQueue<Task> q){
		int result=0;
		Iterator<Task> iterator= q.iterator();
		
		while(iterator.hasNext()){
			
			Task t = iterator.next();
			result += t.completionTime*t.weight;
		}
		return result;
				
	}
	
	public static void printQueue(PriorityQueue<Task> q){
		
		Iterator<Task> iterator= q.iterator();
		
		while(iterator.hasNext()){
			
		System.out.println(iterator.next());
			
		}
	}
	
	

}
