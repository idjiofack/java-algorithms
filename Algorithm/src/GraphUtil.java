import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class GraphUtil {

	public static Edge queueContains(PriorityQueue<Edge> e,Edge i){
		
		for (Edge v : e) {
			if(i.nodeLabel == v.nodeLabel){
				return v;
			}
		}
		return null;
	}
	
public static Edge linkContains(LinkedList<Edge> e,Edge i){
		
		for (Edge v : e) {
			if(i.nodeLabel == v.nodeLabel){
				return v;
			}
		}
		return null;
	}
	
	public static Graph LoadGraph( String filename){
		
		Graph graph= null;
		Hashtable<Integer, LinkedList<Edge>> representation= new Hashtable<>();
		int i=0;
		Path source = Paths.get(filename);
		Charset charset= Charset.forName("US-ASCII");
		try(
				BufferedReader reader = Files.newBufferedReader(source, charset)
				
				)
		
		{
			String line= null;
			
			while ((line= reader.readLine()) != null) {
				//System.out.println(line);
				
				//lines.add(line);
				// create and add tasks to the priority queue
					if(i==0){
						//read the first line of the file and initialize the queue
						String[] values=line.split(" ");
						graph = new Graph(Integer.parseInt(values[0]),Integer.parseInt(values[1]));
						i++;
					}
					else{
						String[] values=line.split(" ");
						int depNode=Integer.parseInt(values[0]);
						int arrNode= Integer.parseInt(values[1]);
						int cost= Integer.parseInt(values[2]);
						LinkedList<Edge> nodeChilds= representation.get(depNode);
						//becuase it is an undirected graph
						LinkedList<Edge> nodeChildsRev= representation.get(arrNode);
						if(nodeChilds==null){
							nodeChilds= new LinkedList<>();	
							nodeChilds.add( new Edge(arrNode,depNode, cost));
							representation.put(depNode, nodeChilds);
						}
						else
						nodeChilds.add( new Edge(arrNode,depNode, cost));
						
						// add the adjacent list for the other part
						if(nodeChildsRev==null){
							nodeChildsRev= new LinkedList<>();	
							nodeChildsRev.add( new Edge(depNode,arrNode, cost));
							representation.put(arrNode, nodeChildsRev);
						}
						else
						nodeChildsRev.add( new Edge(depNode,arrNode, cost));
						
					}
					
				}
				
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		graph.setGraph(representation);
		return graph;
		
	}
	

public static void writeResult(LinkedList<Edge> list, String filename){
	
	Path target = Paths.get(filename);
	Charset charset= Charset.forName("US-ASCII");
	
try ( BufferedWriter writer= Files.newBufferedWriter(target, charset))
	
	
	{
	
	Iterator<Edge> iterator= list.iterator();
	
	while(iterator.hasNext()){
		
		Edge t= iterator.next();
		String s= "Node depart :"+t.parent+"Node arrive "+t.nodeLabel +"cout "+ t.cost;
		writer.append(s, 0, s.length());
		writer.newLine();
	}
	
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


public static Edge getMinNode(PriorityQueue<Edge> e){
	Edge minEdge= new Edge(0, 0, 10010);
	for (Edge x : e) {
		if(x.cost<minEdge.cost)minEdge=x;
	}
	e.remove(minEdge);
	
	
	return minEdge;
}
}


