import java.util.Hashtable;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class Graph {
	int nodes;
	int edges;
	Hashtable<Integer,LinkedList<Edge>> graph;
	 public Graph(int nodes, int edges){
		 this.nodes=nodes;
		 this.edges=edges;
	 }
	 public Hashtable<Integer, LinkedList<Edge>> getGraph() {
		return graph;
	}
	 public void setGraph(Hashtable<Integer, LinkedList<Edge>> graph) {
		this.graph = graph;
	}
}
