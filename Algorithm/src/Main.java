import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class Main {

	static PriorityQueue<Task> queue;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		queue= Util.loadTask("Jobs.txt");
		System.out.println("The size of the queue is "+ queue.size());
		System.out.println("sum of weighted completion time is " +Util.computeCompletionTime(queue));
		
		Util.computeCompletionTime(queue);
		Util.printQueue(queue);*/
		
		
		Graph graph= GraphUtil.LoadGraph("Graph.txt");
		PriorityQueue<Edge> temp = new PriorityQueue<>();
		
		LinkedList<Edge> tree= new LinkedList<>();
		LinkedList<Edge> correspond= new LinkedList<>();
		
		
		//Initialization of nodes starting at node number one(this may be wrong)
		for(int i=1;i<graph.nodes+1; i++){
			
			tree.add(new Edge(i, -1, 10000));
			correspond.addAll(graph.getGraph().get(i));
			temp.add(new Edge(i, -1, 10000+i));
		}
		
		// initiatilization of the first node to 0
		GraphUtil.queueContains(temp, new Edge(1,0,0)).setCost(0);		
		GraphUtil.linkContains(tree, new Edge(1,0,0)).setCost(0);
		
		while(!temp.isEmpty()){
			
			Edge e= GraphUtil.getMinNode(temp);		
			
			
			Hashtable<Integer, LinkedList<Edge>> connexions= graph.getGraph();
			LinkedList<Edge> adjList=connexions.get(e.nodeLabel);
			for (Edge n : adjList) {
				
				Edge v= GraphUtil.queueContains(temp, n);
				if((v != null)&&(n.cost<v.cost)){
					//mettre a jour les deux tables
					GraphUtil.linkContains(tree, v).cost=n.cost;
					GraphUtil.linkContains(tree, v).parent=e.nodeLabel;
					v.cost=n.cost;
					v.parent=e.nodeLabel;
				}
				
			}
			
		
		}
		
		int total=0;
		for (Edge e : tree) {
			total += e.cost;
		}
		
	/*	for (Edge e : tree) {
			System.out.println(e);
		}*/
		System.out.println("This is the total cost of the spanning tree: "+ total);		
			
	
	}

}
